#Unique Characters From A String
#When Input String is Sorted
instring="abbbccdefghhhijkllmmnnoopqrstuvxyzz"
inlength = len(instring) - 1
#print(inlength)
outstring = ""
for i in range(0,inlength):
 if instring[i] != instring[i+1]:
  outstring += instring[i]
  #print(outstring)
 if i == inlength:
  print(instring[i + 1])
print("instring = ",instring)
print("outstring = ",outstring)

#When String is not Sorted
instring = "ABBABBBBBDIIOEBAIDOBCOWGONSVOOWNBAZZZWDBSBaaaarrrrratataggeyvstwztaie16337265$$$#@&@&"
outstring = ""
for i in instring:
    flag = 0
    for j in outstring:
        if i == j:
            flag +=1
    if flag == 0:
        outstring += i
print("instring = ",instring)
print("outstring = ",outstring)